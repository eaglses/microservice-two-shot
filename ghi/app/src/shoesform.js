import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [ bin, setbin ] = useState([])
    const [ formData, setFormData ] = useState({
        manufacturer: '',
        model: '',
        color: '',
        picture_url: '',
        bin: '',
    })

    const fetchData = async () => {
        const url = `http://localhost:8100/api/bins`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            
            setbin(data.bins);
        }
        console.log("fetch data", response)
    }

useEffect(() => {
    fetchData();
}, []);

const handleSubmit = async (event) => {
    event.preventDefault();

    const url = `http://localhost:8080/api/shoes/`;

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(url, fetchConfig);
    console.log("post", response)
    if (response.ok) {
        setFormData({
            manufacturer: '',
            model: '',
            color: '',
            picture_url: '',
            bin: '',
        });
        document.getElementById("create-shoe-form").reset();
    }
}

const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
        ...formData,
        [inputName]: value
    });
}

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>List new shoes</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.manufacturer}placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.model} placeholder="model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="model">model</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture url" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture url</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bin.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
)

}

export default ShoeForm;
