import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ShoesMain from "./shoesMain";
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './shoesform'
import HatList from './HatList';
import HatForm from './HatForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesMain shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>

  );
  }

export default App;
