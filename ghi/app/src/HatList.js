import React, { useEffect, useState} from "react";
import { Link } from 'react-router-dom';

const deleteHat = async(event) => {
  event.preventDefault();
  const id = event.target.value;
  const url = `http://localhost:8090/api/hats/${id}/`
  const fetchConfig = {
    method: "DELETE",
    body: '',
    headers: {
      "Content-Type": "application/json",
    },
  };
  const response = await fetch(url, fetchConfig);
  if (response.ok) {
    console.log("Hat deleted");
  } else {
    console.log("Hat could not be deleted")
  }
}

function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        const hat = data.hat[0];
        return (
          <div key={hat.id} className="card mb-3 shadow">
            <img
              src={hat.picture_url}
              className="card-img-top"
            />
            <div className="card-body">
              <h5 className="card-title">Hat {hat.id}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                Fabric: {hat.fabric}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Closet: {hat.location.closet_name}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Color: {hat.color}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Style: {hat.style_name}
              </h6>
              <button type="button" value={hat.id} onClick={(event) => deleteHat(event, hat.id)} className="btn btn-danger">Delete</button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const MainPage = (props) => {
    const [hatColumns, setHatColumns] = useState([[], [], []]);
  
    async function getHats() {
      const url = `http://localhost:8090/api/hats/`;
  
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
  
          const requests = [];
          for (let hat of data.hats) {
            
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}/`;
          requests.push(fetch(detailUrl));
          }
  
          const responses = await Promise.all(requests);
          const hatColumns = [[], [], []];
  
          let i = 0;
          for (const hatResponse of responses) {
            if (hatResponse.ok) {
              const details = await hatResponse.json();
              hatColumns[i].push(details);
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(hatResponse);
            }
          }
          setHatColumns(hatColumns);
        }
      } catch (e) {
        console.error(e);
      }
    }
  
  useEffect(() => {
    getHats();
  }, []);
  
    return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Keep track of your hats!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a hat</Link>
          </div>
        </div>
      </div>
      <div className="container">
          <h2>Your Hats</h2>
          <div className="row">
            {hatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList} />
              );
            })}
          </div>
      </div>
    </>
    );
  }
  
  export default MainPage