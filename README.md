# Wardrobify

Team:

* Daniel Gay - Shoes
* Roland Manvelyan - Hats

## Design

## Shoes microservice
model shoe will have a many to one relationship with bins.

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

The hat model represents a hat entity, including attributes like fabric, style name, color, picture Url, and a foreign key relationship with "LovationVO", indicating where the hat is stored. The hat microservice and the wardrobe microservice integrate by sharing similar location concepts. The wardrobe manages physical storage spaces, while the hats_api handles hat-specific details and their associations with those storage locations.