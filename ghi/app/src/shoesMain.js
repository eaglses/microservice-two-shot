import React, { useEffect, useState} from "react";
import { Link } from 'react-router-dom';

const deleteShoe = async(event) => {
  event.preventDefault();
  const id = event.target.value;
  const url = `http://localhost:8080/${id}`
  console.log(id, url)
  const fetchConfig = {
    method: "DELETE",
    body: '',
    headers: {
      "Content-Type": "application/json",
    },
  };
  const response = await fetch(url, fetchConfig);
  if (response.ok) {
    console.log("Shoes deleted");
    
  } else {
    console.log("Shoes could not be deleted")
  }
}

function ShoeColumn(props) {
  return (
    <div className="col">
      
      {props.list.map(data => {        
        const shoe = data.shoe[0];
        
        return (
          <div key={shoe.href} className="card mb-3 shadow">
            <img
              src={shoe.picture_url}
              className="card-img-top"
            />
            <div className="card-body">
              <h5 className="card-title">Shoe Model {shoe.model}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
              Manufacturer: {shoe.manufacturer}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Color: {shoe.color}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Closet: {shoe.bin.closet_name}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Bin: {shoe.bin.bin_number}
              </h6>
              <button type="button" value={shoe.href} onClick={(event) => deleteShoe(event, shoe.href)} className="btn btn-danger">Delete</button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const ShoesMain = (props) => {
    const [ShoeColumns, setShoeColumns] = useState([[], [], []]);
  
    async function getShoes() {
      const url = `http://localhost:8080/api/shoes/`;
  
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
            //_________________ 
          const requests = [];
          for (let shoe of data.Shoes) {
            
          const detailUrl = `http://localhost:8080${shoe.href}`;
          requests.push(fetch(detailUrl));
          }
  
          const responses = await Promise.all(requests);
          const ShoeColumns = [[], [], []];
  
          let i = 0;
          
          for (const shoeResponse of responses) {
            if (shoeResponse.ok) {
              const details = await shoeResponse.json();

              ShoeColumns[i].push(details);              
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(shoeResponse);
            }
          }
          setShoeColumns(ShoeColumns);
        }
      } catch (e) {
        console.error(e);
      }
    }
  
  useEffect(() => {
    getShoes();
  }, []);
  
    return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Keep track of your all those pesky pairs of Shoes!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/Shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add new shoes</Link>
          </div>
        </div>
      </div>
      <div className="container">
          <h2>Shoes</h2>
          <div className="row">
            {ShoeColumns.map((shoeList, index) => {
              return (
                <ShoeColumn key={index} list={shoeList} />
              );
            })}
          </div>
      </div>
    </>
    );
  }
  
  export default ShoesMain

        











// function ShoesMain(props) {

// return (
//   <table className="table table-striped">
//     <thead>
//       <tr>
//         <th>shoe</th>
//         <th>bin number</th>
//       </tr>
//     </thead>
//     <tbody>
//         {props.shoes.map(shoe => {
//           return (
//             <tr key={shoe.href}>
//               <td>{ shoe.model }</td>
//               <td>{ shoe.bin_number }</td>
//               <td>{ shoe.bin_number }</td>
//             </tr>
//           );
//         })}
//     </tbody>
//   </table>
// );
// }
  
  
//   export default ShoesMain;
