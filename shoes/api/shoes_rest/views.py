from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

from common.json import ModelEncoder
from shoes_rest.models import Shoe, BinVO


class BinEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture_url",
        "bin",
        
    ]

    encoders = {
        "bin": BinEncoder(),
    }


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model",
    ]
    def get_extra_data(self, o):
        return{"bin_number": o.bin.bin_number}
    
    


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
        if request.method == "GET":
            Shoes = Shoe.objects.all()
            return JsonResponse(
                 {"Shoes": Shoes},
                 encoder=ShoeListEncoder,
                 safe=False,
            )
        else:
            content = json.loads(request.body)
            
         
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)

            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.filter(id=pk)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()

        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            safe=False,
        )
